package com.tanzer.insurance01;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class InsuranceFileCliRunner implements CommandLineRunner {

	@Autowired
	InsuranceFileProcess insuranceFileProcess;
	
	@Override
	public void run(String... args) throws Exception {
		for(final String arg : args) {
			insuranceFileProcess.process(arg);
		}
	}

}
