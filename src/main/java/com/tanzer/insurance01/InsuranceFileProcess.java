package com.tanzer.insurance01;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

@Service
public class InsuranceFileProcess {

	@Autowired
	CsvMapper csvMapper;

	public boolean process(String name) throws IOException {

		List<InsuranceUser> users = readInputFile(name);

		Map<String, String> insuranceCompanies = new HashMap<>();
		for (InsuranceUser user : users) {
			insuranceCompanies.put(user.getInsuranceCompany(), user.getInsuranceCompany());
		}

		for (Map.Entry<String, String> entry : insuranceCompanies.entrySet()) {
			buildOutputFile(entry.getKey(), users);
		}

		return true;
	}

	List<InsuranceUser> readInputFile(String name) throws IOException {
		File inputFile = new File(name);
		CsvSchema csvSchema = csvMapper.typedSchemaFor(InsuranceUser.class).withColumnSeparator(',');
		MappingIterator<InsuranceUser> userIter = csvMapper.readerWithTypedSchemaFor(InsuranceUser.class).with(csvSchema).readValues(inputFile);

		return userIter.readAll();
	}
	
	File buildOutputFile(String insuranceName, List<InsuranceUser> users) throws IOException {
		File outputFile = new File(insuranceName.replace(" ", "").trim() + ".csv");
		
		List<InsuranceUser> usersPruned = removeDupes(users);
		
		Comparator<InsuranceUser> comparator = Comparator.comparing(InsuranceUser::getLastName);
		comparator = comparator.thenComparing(Comparator.comparing(InsuranceUser::getFirstName));
		usersPruned.sort(comparator);

		CsvMapper mapper = new CsvMapper();
		mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true);
		CsvSchema csvSchema = csvMapper.typedSchemaFor(InsuranceUser.class).withColumnSeparator(',').withoutHeader();
		ObjectWriter writer = mapper.writerFor(InsuranceUser.class).with(csvSchema);
		writer.writeValues(outputFile).writeAll(usersPruned.stream().filter(p -> p.getInsuranceCompany().contentEquals(insuranceName)).collect(Collectors.toList()));
		
		System.out.print("Saved output file: ");
		System.out.println(outputFile.getAbsolutePath());
		return outputFile;
	}
	
	List<InsuranceUser> removeDupes(List<InsuranceUser> users) {
		Comparator<InsuranceUser> comparator = Comparator.comparing(InsuranceUser::getUserId);
		comparator = comparator.thenComparing(Comparator.comparing(InsuranceUser::getInsuranceCompany));
		comparator = comparator.thenComparing(Comparator.comparing(InsuranceUser::getVersion).reversed());
		// the sort comparison on version is reversed so newer versions are first
		users.sort(comparator);
		
		// if there are duplicate InsuranceUser Ids for the same Insurance Company, then only
		// the record with the highest version should be included - the distinctByKey will return the first of the dupes
		return users.stream().filter( distinctByKey( u -> u.getUserId() + " " + u.getInsuranceCompany() )).collect(Collectors.toList());
	}
	
	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) 
	{
	  Map<Object, Boolean> map = new ConcurrentHashMap<>();
	  return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}
	
}
