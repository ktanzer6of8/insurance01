package com.tanzer.insurance01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Insurance01Application {

	public static void main(String[] args) {
		SpringApplication.run(Insurance01Application.class, args);
	}

}
