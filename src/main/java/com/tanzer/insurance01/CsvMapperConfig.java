package com.tanzer.insurance01;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;

@Configuration
public class CsvMapperConfig {

	@Bean
	public CsvMapper csvMapper() {
		return new CsvMapper();
	}
}
