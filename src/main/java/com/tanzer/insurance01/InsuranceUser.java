package com.tanzer.insurance01;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"userId", "firstName", "lastName", "version", "insuranceCompany"})
public class InsuranceUser {

	String userId;
	String firstName;
	String lastName;
	int version;
	String insuranceCompany;
	
	// additional setter so jacksonxml can convert the String to int
	public void setVersion(String intString) {
		this.version = new Integer(intString);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) 
			return true;
		if(obj  == null) return false;
		if (getClass() != obj.getClass()) return false;
		InsuranceUser other = (InsuranceUser) obj;
		return (Objects.equals(userId, other.getUserId()) && 
				Objects.equals(insuranceCompany, other.getInsuranceCompany()) &&
				Objects.equals(version, other.getVersion()) );
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((insuranceCompany == null) ? 0 : insuranceCompany.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + version;
		return result;
	}

}
