package com.tanzer.insurance01;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
class InsuranceFileProcessTest {

	@Autowired
	InsuranceFileProcess insFileProcess;
	
	List<InsuranceUser> listUsers;
	
	@BeforeEach
	void setUp() throws Exception {
		listUsers = new ArrayList<>();

		InsuranceUser user3 = new InsuranceUser();
		user3.setFirstName("first2");
		user3.setLastName("last2");
		user3.setUserId("user2");
		user3.setInsuranceCompany("insurance2");
		user3.setVersion(Integer.toString(2));
		listUsers.add(user3);

		InsuranceUser user1 = new InsuranceUser();
		user1.setFirstName("first1");
		user1.setLastName("last1");
		user1.setUserId("user1");
		user1.setInsuranceCompany("insurance1");
		user1.setVersion(Integer.toString(1));
		listUsers.add(user1);

		InsuranceUser user2 = new InsuranceUser();
		user2.setFirstName("first2");
		user2.setLastName("last2");
		user2.setUserId("user2");
		user2.setInsuranceCompany("insurance2");
		user2.setVersion(Integer.toString(1));
		listUsers.add(user2);

		InsuranceUser user4 = new InsuranceUser();
		user4.setFirstName("first2");
		user4.setLastName("last2");
		user4.setUserId("user2");
		user4.setInsuranceCompany("insurance2");
		user4.setVersion(Integer.toString(3));
		listUsers.add(user4);
}

	@Test
	void test_ReadFile_insurance1() throws URISyntaxException, IOException {
		URL url = this.getClass().getClassLoader().getResource("insurance1.csv");
		File inFile = new File(url.toURI());
		List<InsuranceUser> foundUsers = insFileProcess.readInputFile(inFile.getPath());
		assertEquals(9, foundUsers.size());
	}
	

	@Test
	void test_BuildOutputFile_dummy() throws IOException {
		File outfile = insFileProcess.buildOutputFile("dummy", new ArrayList<InsuranceUser>());
		assertTrue(outfile.exists());
		assertEquals(0, outfile.length());
		outfile.delete();
	}

	@Test
	void test_BuildOutputFile_single() throws IOException {
		File outfile = insFileProcess.buildOutputFile("insurance1", listUsers);
		assertTrue(outfile.exists());
		assertEquals(32, outfile.length());
		outfile.delete();
	}

	@Test
	void test_BuildOutputFile_multiples() throws IOException {
		File outfile = insFileProcess.buildOutputFile("insurance2", listUsers);
		assertTrue(outfile.exists());
		assertEquals(32, outfile.length());
		outfile.delete();
	}
	
	@Test
	void test_removeDupes() {
		List<InsuranceUser> pruned = insFileProcess.removeDupes(listUsers);
		assertEquals(2, pruned.size());
	}
}
